Workflow (Plastic SCM + GitStack + GitLab)

1. Commit changes using Plastic SCM Gluon
2. Push changes by clicking RefreshLocalHost.bat in RepoSetting inside PlasticSCM Gluon Explorer
3. Changes Pushed to Gitlab  